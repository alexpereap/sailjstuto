window.onload = function subscribeAndListen(){

	// When the document loads, send a request to users.testSocket
    // The controller code will subscribe you to all of the 'users' model instances (records)
	io.socket.get('/user/subscribe/');

	io.socket.on('user',function(obj){


      if (obj.verb == 'updated') {
        var previous = obj.previous;
        var data = obj.data;

        console.log(previous);
        console.log(data);
        // console.log('User '+previous.online+' has been updated to '+data.online);
      }
    });

};