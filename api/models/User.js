/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,
  connection: 'someMysqlServer',

  attributes: {

    name: {
        type: 'string',
        required: true
    },
    title: {
        type: 'string'
    },
    email: {
        type: 'email',
        required: true,
        unique: true
    },
    encryptedPassword: {
        type: 'string'
    },

    online: {
      type : 'boolean',
      defaultsTo: false
    },

    admin:{
        type     : 'boolean',
        defaultsTo: false
    }
  },
  beforeValidation: function (values, next) {

    console.log("Before validation doing shit");
    /*console.log(values);
    if( typeof values.admin === 'undefined' ){
        values.admin = false;
    } else {
        values.admin = true;
    }*/
    
    next();
  },

  beforeCreate: function(values,next){

    // This checks to make sure the password and password confirmation match before creating a record
    if( !values.encryptedPassword || values.encryptedPassword != values.confirmation ){
        return next({ err: ["Password doesn't match password confirmation"] });
    }

    require('bcrypt').hash(values.encryptedPassword, 10, function passwordEncrypted(err,encryptedPassword){
        if(err) return next(err);
        values.encryptedPassword = encryptedPassword;
        next();
    });
  }
};