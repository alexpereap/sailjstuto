module.exports = function(req,res,ok){

    var sessionUserMatchesId = req.session.User.id == req.param('id');
    var isAdmin = req.session.User.admin;

    console.log("usercanSeeProfile policie called: user id: " + req.session.User.id + "request uid: " + req.param('id') );

    if( !(sessionUserMatchesId || isAdmin) ){
        var noRightsError = [{name:'noRights', message: 'You must be an admin'}];

        req.session.flash = {
            err: noRightsError
        }

        res.redirect('/session/new');
        return;
    }

    ok();
}