/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    'new': function(req,res){
        res.view();
    },

    create: function(req, res, next){

        // Create a user with the params sent from
        // the sign-up form --> new.ejs

        User.create( req.params.all(), function userCreated(err, user){

            // if there's an error
            if(err) {

                console.log(err);
                // if error redirect back to sign-up page
                // return next(err)

                req.session.flash = {
                    err: err
                };

                return res.redirect('/user/new');
            }

            // After successfully creating the user
            // redirect to the show action
            // res.json(user);

            //Log in user
            req.session.authenticated = true;
            req.session.User = user;

            // status go to online
            user.online = true;
            user.save(function(err,user){
                if( err ) return netxt(err);

                return res.redirect('/user/show/' + user.id );
            });

        });
    },

    // render the profile view (eg. /views/show.ejs)
    show: function( req, res, next ){
        User.findOne(req.param('id'), function foundUser(err, user){

            if (err) return next(err);
            if(!user) return next();

            res.view({
                user: user
            });

        });
    },

    index: function( req, res, next ){

        User.find(req.param('id'), function foundUsers(err, users){

            if (err) return next(err);

            res.view({
                users: users
            });

        });
    },

    edit: function( req, res, next ){
        User.findOne(req.param('id'), function foundUser(err, user){

            if (err) return next(err);
            if( !user ) return next('User doesn\'t exsist.');

            res.view({
                user: user
            });

        });
    },

    update: function( req, res, next ){
        User.update(req.param('id'), req.params.all(), function foundUser(err){

            if(err){
                return res.redirect('/user/edit/' + req.param('id') );
            }

            res.redirect('/user/show/' + req.param('id'));
        });
    },

    destroy: function( req, res, next ){
        User.findOne(req.param('id'), function foundUser(err, user){

            if( err ) return next(err);

            if(!user) return next('User doesn\'t exsist.');

            User.destroy(req.param('id'), function userDestroyed(){
                if(err) return next(err);
            });

            res.redirect('/user');
        });
    },

    subscribe: function(req, res){

        User.find(function foundUsers(err,users){
            if(err) return next(err);

            User.subscribe(req.socket,users);
            console.log('User with socket id '+req.socket.id+' is now subscribed to all of the model instances in \'users\'.');
        });
    }
};

