/**
 * SessionController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var bcrypt = require('bcrypt');

module.exports = {
	'new': function(req,res){

        res.view('session/new');
    },
    'create': function(req,res,next){

        // check filled fields
        if( !req.param('email') || !req.param('password') ){
            var usernamePasswordRequiredError = [{name: 'usernamePasswordRequred', message:'You must enter both a username and password'}];

            req.session.flash = {
                err: usernamePasswordRequiredError
            };

            res.redirect('/session/new');
            return;
        }

        User.findOneByEmail( req.param('email'), function foundUser(err,user){

            if(err) return next(err);

            // check if email exists
            if(!user){
                var noAccountError = [{ name: 'noAccount', message: 'The email address ' + req.param('email') + ' not found.' }];
                req.session.flash = {
                    err: noAccountError
                };

                res.redirect('/session/new');
                return;
            }

            // compare password and email
            bcrypt.compare(req.param('password'), user.encryptedPassword, function(err, valid){
                if(err) return next(err);

                if(!valid){
                    var usernamePasswordMissMatcherror = [{ name: 'usernamePasswordMissMatch', message: 'Invalid username and password combination.' }];
                    req.session.flash = {
                        err: usernamePasswordMissMatcherror
                    };

                    res.redirect('/session/new');
                    return;
                }

            });

            // Log user in
            req.session.authenticated = true;
            console.log(user);
            req.session.User = user;

            // status go to online
            user.online = true;
            user.save(function(err,user){
                if( err ) return netxt(err);

                // inform other sockets (e.g. connected sockets that are subscirbed that this user is now logged in)
                User.publishUpdate(user.id,{ online: true, id: user.id });

                // if the user is an admin redirects to the user list
                if( req.session.User.admin ){
                    res.redirect('/user');
                    return;
                }

                //Redirect to their profile page (e,g, /views/user/shows.ejs)
                res.redirect('/user/show/' + user.id);
            });


        });
    },

    'destroy': function(req,res,next){

        User.findOne(req.session.User.id, function foundUser(err,user){

            var userId = req.session.User.id;

            User.update(userId,{
                online:false
            }, function(err){
                if(err) return next(err);

                // wipe out the session
                req.session.destroy();

                // redirect browser to the sign in screen
                res.redirect('/session/new');

            });

        });
    }
};

